# Appmoto showroomwebsites

This repository contains the actual built showroom websites for Dusseldorp BMW / MINI and RENOVA BMW / MINI.

### Permission
This repository can only be used when the server's public key is added to the repositories access keys (https://bitbucket.org/freshapple/showroomwebsites/admin/access-keys/). Without permissions, the repository is not accessible.

### Usage
To use this repo, it has to be added to the composer.json via the following code:
```
"repositories": [
    {
        "type": "vcs",
        "url": "git@bitbucket.org:freshapple/showroomwebsites.git"
    }
]
```
After that, it can be required via `composer require freshapple/showroomwebsites`. 
New versions of the package will be required when running `composer update`.

### General
This repository distributes the built Vue applications to Cartel (https://www.cartel.nl/) for deployment on various BMW / MINI websites.

### Contact
Fresh Apple Media Architects
Thom - 055-73701651
thom@freshapple.nl